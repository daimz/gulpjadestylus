/* jshint node:true */
'use strict';
// generated on 2015-02-09 using generator-gulp-webapp 0.2.0
var gulp = require('gulp');
var stylus = require('gulp-stylus');
var $ = require('gulp-load-plugins')();
var svgstore = require('gulp-svgstore');
var svgmin = require('gulp-svgmin');
var cheerio = require('gulp-cheerio');
var rename = require('gulp-rename');
var sourcemaps = require('gulp-sourcemaps');
var jeet = require('jeet');
var rupture = require('rupture');
var koutoSwiss = require( "kouto-swiss" );
var data = require('gulp-data');
var path = require('path');
var fs   = require('fs');

//gulp.task('styles', function () {
//	return gulp.src('app/styles/main.scss')
//		.pipe($.plumber())
//		//    .pipe($.rubySass({
//		//      style: 'expanded',
//		//      precision: 10
//		//    }))
//		.pipe($.sass({
//			outputStyle: 'nested', // libsass doesn't support expanded yet
//			precision: 10,
//			includePaths: ['.'],
//			onError: console.error.bind(console, 'Sass error:')
//		}))
//		.pipe($.autoprefixer({
//			browsers: ['last 2 version']
//		}))
//		.pipe(gulp.dest('.tmp/styles'));
//});

gulp.task('styles', function () {
	gulp.src('app/styles/main.styl')
		.pipe(sourcemaps.init())
		.pipe(stylus({
			use: [jeet(), rupture(), koutoSwiss()],
			compress: true,
      linenos: true
		}))
		.pipe($.autoprefixer({
			browsers: ['last 2 version']
		}))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('.tmp/styles'));
});

gulp.task('views', function () {
	return gulp.src('app/{,*/}*.jade')
		.pipe(data(function(file) {
      return require('app/data/data.json');
    }))
		.pipe($.jade({
			pretty: true,
			basedir: 'app'
		}))
		.pipe(gulp.dest('.tmp'));
});

gulp.task('jshint', function () {
	return gulp.src('app/scripts/**/*.js')
		.pipe($.jshint())
		.pipe($.jshint.reporter('jshint-stylish'))
		.pipe($.jshint.reporter('fail'));
});

gulp.task('html', ['views', 'styles'], function () {
	var assets = $.useref.assets({
		searchPath: '{.tmp,app}'
	});

	return gulp.src(['app/*.html', '.tmp/*.html'])
		.pipe(assets)
		.pipe($.if('*.js', $.uglify()))
		.pipe($.if('*.css', $.csso()))
		.pipe(assets.restore())
		.pipe($.useref())
		.pipe($.if('*.html', $.minifyHtml({
			conditionals: true,
			loose: true
		})))
		.pipe(gulp.dest('dist'));
});

gulp.task('images', function () {
	return gulp.src('app/images/**/*')
		.pipe($.cache($.imagemin({
			progressive: true,
			interlaced: true
		})))
		.pipe(gulp.dest('dist/images'));
});

gulp.task('svg', function () {
	return gulp
		.src('app/svg/icons/*.svg', {
			base: 'icons'
		})
		.pipe(svgmin())
		.pipe(rename({
			prefix: 'icon-'
		}))
		.pipe(cheerio({
			run: function ($) {
				$('[fill]').removeAttr('fill');
				$('svg').attr('style', 'display:none');
			},
			parserOptions: {
				xmlMode: true
			}
		}))
		.pipe(svgstore({
			inlineSvg: true
		}))
		.pipe(gulp.dest('app/svg'));
});

gulp.task('svgmove', function () {
	return gulp.src(['app/svg/*.svg', '!app/svg/icons/*.svg'])
		.pipe(gulp.dest('dist/svg'));
});

gulp.task('fonts', function () {
	return gulp.src(require('main-bower-files')().concat('app/fonts/**/*'))
		.pipe($.filter('**/*.{eot,svg,ttf,woff}'))
		.pipe($.flatten())
		.pipe(gulp.dest('dist/fonts'));
});

gulp.task('extras', function () {
	return gulp.src([
    'app/*.*',
    '!app/*.html',
    '!app/*.jade',
    'node_modules/apache-server-configs/dist/.htaccess'
  ], {
		dot: true
	}).pipe(gulp.dest('dist'));
});

gulp.task('clean', require('del').bind(null, ['.tmp', 'dist']));

gulp.task('connect', ['views', 'styles', 'svg'], function () {
	var serveStatic = require('serve-static');
	var serveIndex = require('serve-index');
	var app = require('connect')()
		.use(require('connect-livereload')({
			port: 35729
		}))
		.use(serveStatic('.tmp'))
		.use(serveStatic('app'))
		// paths to bower_components should be relative to the current file
		// e.g. in app/index.html you should use ../bower_components
		.use('/bower_components', serveStatic('bower_components'))
		.use(serveIndex('app'));

	require('http').createServer(app)
		.listen(9000)
		.on('listening', function () {
			console.log('Started connect web server on http://localhost:9000');
		});
});

gulp.task('serve', ['connect', 'watch'], function () {
	require('opn')('http://localhost:9000');
});

// inject bower components
gulp.task('wiredep', function () {
	var wiredep = require('wiredep').stream;

	gulp.src('app/styles/*.styl')
		.pipe(wiredep({
			exclude: ['bootstrap-sass-official'],
			ignorePath: /^(\.\.\/)*\.\./
		}))
		.pipe(gulp.dest('app/styles'));

	gulp.src('app/layouts/*.jade')
		.pipe(wiredep({
			exclude: ['bootstrap-sass-official'],
			ignorePath: /^(\.\.\/)*\.\./
		}))
		.pipe(gulp.dest('app'));
});

gulp.task('watch', ['connect'], function () {
	$.livereload.listen();

	// watch for changes
	gulp.watch([
    'app/*.html',
    '.tmp/*.html',
    '.tmp/styles/**/*.css',
    'app/scripts/**/*.js',
    'app/images/**/*',
    'app/**/*.svg'
  ]).on('change', $.livereload.changed);

	gulp.watch('app/**/*.jade', ['views']);
	gulp.watch('app/styles/**/*.styl', ['styles']);
	gulp.watch('app/**/*.svg', ['svg']);
	gulp.watch('bower.json', ['wiredep']);
});

gulp.task('build', ['jshint', 'html', 'images', 'svg', 'svgmove', 'fonts', 'extras'], function () {
	return gulp.src('dist/**/*').pipe($.size({
		title: 'build',
		gzip: true
	}));
});

gulp.task('default', ['clean'], function () {
	gulp.start('build');
});